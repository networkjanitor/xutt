Xutt
====

This is a collections of datasets for Guildwars 2 events, for example the Triple Trouble Event or The Shatterer, adjusted to OpenCommunity style and to be used in combination with the `bronk <https://gitlab.com/networkjanitor/bronk>`_ module (linux client).

There are also static html pages generated with `canni <https://gitlab.com/networkjanitor/canni>`_: https://networkjanitor.gitlab.io/xutt

What all this is about
----------------------

This is all about reducing the number of keypresses, which are required to copy one message into the game chat.

The 'usual' setup
`````````````````

* two monitors
* GuildWars2 in windowed/borderless mode
* monitor 1: GuildWars2
* monitor 2: Excel sheet with one message per cell

The 'usual' workflow
````````````````````

* move mouse out of GuildWars2 into Excel sheet
* click on cell you want to copy (select data)
* press Ctrl+C to copy message
* move mouse back into GuildWars2
* click in game chat
* press Ctrl+V to paste message
* press Enter to write message

The improvement
```````````````

* replace Excel sheet with either `the html page <https://networkjanitor.gitlab.io/xutt>`_ or a desktop application `bronk <https://gitlab.com/networkjanitor/bronk>`_ to display the data
* instead of being required to click on the data (select) and press Ctrl+C to copy it, now you only need to click on the data (select)

Future optimisations
````````````````````

While this workflow is acceptable for posting explanations during the dry run, it would be advantegous to be able to set
hotkeys to copy certain messages into the clipboard (especially during the actual run/event).

This would shorten the workflow to the following steps:

* press hotkey combination to copy a preset message into clipboard (e.g.: Ctrl+Shift+2)
* click into game chat
* press Ctrl+V to paste message
* press Enter to write message

Much better. Disadvantages of this:

* needs actual desktop application to capture hotkeys (windows: TODO / linux: `bronk <https://gitlab.com/networkjanitor/bronk>`_)



How2Use
-------

#. clone repository: `git clone <repository>`
#. create and activate virtual environment (optional, but recommended): `python3 -m venv venv && source venv/bin/activate`
#. install requirements (bronk): `pip install -r requirements.txt`
#. run it: `python3 -m bronk tripletrouble`

Name Origin
-----------

One of the involved asuras of the Triple Trouble event is called Crusader *Xutt*.