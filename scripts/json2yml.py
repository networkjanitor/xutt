#!/usr/bin/env python3
import sys, yaml, json, os
from pathlib import Path
for dirpath in [x for x in Path("./json_sources").rglob('*') if x.is_dir()]:
    os.makedirs(Path('yaml_sources') / dirpath, exist_ok=True)
for jpath in [x for x in Path("./json_sources").rglob('*.json') if x.is_file()]:
    print(jpath)
    with open(str(jpath), 'r') as f:
        j = json.load(f)

    jpath = jpath.with_suffix('.yml')

    for k,v in j.items():
        new_yml_msg = []
        for name, message in v.items():
            message['name'] = name
            new_yml_msg.append(message)

        new_yml = {
            'messages': new_yml_msg
        }


        with open(str(Path('yaml_sources') / jpath), 'w') as f:

            yaml.dump(new_yml, f, default_flow_style=False)



        # only one key allowed anyway, so let's break here
        break