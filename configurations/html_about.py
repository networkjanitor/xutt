from configurations.baseconf import BaseConf, BaseSwitch
from settings import Settings


class Switch(BaseSwitch):
    def __init__(self):
        super().__init__()
        self.add_tab(Conf())


class Conf(BaseConf):
    name = 'About'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['html-about'])
        self._calculate_choices()