from configurations.tripletrouble.tripletrouble_voicecommand import Switch as ttswitch
import configurations.html_about as htmlaboutconf


class Switch(ttswitch):
    def __init__(self):
        super().__init__()
        self.add_tab(htmlaboutconf.Conf())