from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'VC Orga'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['tripletrouble']['voicecommand']['orga'])
        self._calculate_choices()
