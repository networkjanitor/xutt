import configurations.tripletrouble.cobalt as cobaltconf
import configurations.tripletrouble.crimson as crimsonconf
import configurations.tripletrouble.amber as amberconf

import configurations.tripletrouble.amber_checklist as amber_cl
import configurations.tripletrouble.cobalt_checklist as cobalt_cl
import configurations.tripletrouble.crimson_checklist as crimson_cl

import configurations.opencommunity as occonf
import configurations.squadjoins as sqconf

import configurations.tripletrouble.orga as orgaconf
import configurations.tripletrouble.orga_pre as orgapreconf
import configurations.tripletrouble.orga_checklist as orga_cl

from configurations.baseconf import BaseSwitch


class Switch(BaseSwitch):
    def __init__(self):
        super().__init__()
        self.add_tab(amberconf.Conf())
        self.add_tab(amber_cl.Conf())
        self.add_tab(cobaltconf.Conf())
        self.add_tab(cobalt_cl.Conf())
        self.add_tab(crimsonconf.Conf())
        self.add_tab(crimson_cl.Conf())
        self.add_tab(occonf.Conf())
        self.add_tab(orgaconf.Conf())
        self.add_tab(orgapreconf.Conf())
        self.add_tab(orga_cl.Conf())
        self.add_tab(sqconf.Conf())



