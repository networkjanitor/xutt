from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'CL Crimson'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['checklist']['pre-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['wurmattacks'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['burn'])
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['checklist']['post-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['wurmhead'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['checklist']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['crimson']['checklist']['eventtalk'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['aftermath'])
        self._calculate_choices()
