from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'CL Cobalt'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['checklist']['pre-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['wurmattacks'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['burn'])
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['checklist']['post-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['wurmhead'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['checklist']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['checklist']['eventtalk'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['checklist']['aftermath'])
        self._calculate_choices()
