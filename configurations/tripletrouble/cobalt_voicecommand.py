from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'VC Cobalt'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['voicecommand']['pre-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['wurmattacks'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['burn'])
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['voicecommand']['post-burn'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['wurmhead'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['voicecommand']['escort'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['burn_broadcasts'])
        self._load_json(Settings.json_sources['tripletrouble']['cobalt']['voicecommand']['post-burn_broadcasts'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['phase2_broadcasts'])
        self._load_json(Settings.json_sources['tripletrouble']['dryrun']['voicecommand']['aftermath'])
        self._calculate_choices()
