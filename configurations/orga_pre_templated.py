from configurations.baseconf import BaseConf
from settings import Settings


class Conf(BaseConf):
    name = 'Orga (Pre)'

    def __init__(self, replacements, start_seq='{{', end_seq='}}'):
        super().__init__()
        self.templ_replacements = replacements
        self.templ_start_seq = start_seq
        self.templ_end_seq = end_seq

        self._load_json(Settings.json_sources['orga-pre'])
        self._calculate_choices()

