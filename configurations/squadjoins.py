from configurations.baseconf import BaseConf
from settings import Settings
import collections


class Conf(BaseConf):
    name = 'Squadjoins'

    def __init__(self):
        super().__init__()
        self._load_json(Settings.json_sources['oc-squadjoins'], self.taxi_mod, self.taxi_sort)
        self._calculate_choices()

    @staticmethod
    def taxi_mod(entry, original_entry_key):
        modified_entries = collections.OrderedDict()

        if str(entry['title']).startswith(" "):
            modified_entries[original_entry_key] = entry
        else:
            mod_entry = collections.OrderedDict(entry).copy()

            mod_entry['title'] = f"Taxi (short) {entry['title']}"
            mod_entry['text'] = []
            for txt in entry['text']:
                mod_entry['text'].append(f"Taxi: {txt}")
            modified_entries[original_entry_key + '_short_taxi'] = mod_entry.copy()

            mod_entry['title'] = f"Taxi (no space) {entry['title']}"
            mod_entry['text'] = []
            for txt in entry['text']:
                mod_entry['text'].append("{}".format(txt))
            modified_entries[original_entry_key + '_no_space_taxi'] = mod_entry.copy()

        return modified_entries

    @staticmethod
    def taxi_sort(choices_list):
        for part in choices_list:
            choices_list[part] = collections.OrderedDict(sorted(choices_list[part].items(), key=lambda t: t[1]['title']))
        return choices_list
