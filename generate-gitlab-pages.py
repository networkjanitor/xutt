#!/usr/bin/env python3
import canni

if __name__ == '__main__':
    shg = canni.StaticHtmlGenerator(output_directory='gitlab-pages', conf_path='data/xutt.yml')
    shg.perform_checks()
    shg.render()
    # shg.render_switch('tripletrouble.amber', output_filename='tt/amber.html', pagetitle='Amber Wurm - Triple Trouble')
    # shg.render_switch('tripletrouble.cobalt', output_filename='tt/cobalt.html', pagetitle='Cobalt Wurm - Triple Trouble')
    # shg.render_switch('tripletrouble.crimson', output_filename='tt/crimson.html', pagetitle='Crimson Wurm - Triple Trouble')
    # shg.render_switch('tripletrouble.tripletrouble_voicecommand_html', output_filename='tt_vc/tripletrouble.html',
    #                   pagetitle='Triple Trouble - OpenCommunity Style (for voice commanding)')
    shg.render_index(output_filename='index.html', pagetitle='Copypasta Index')
